#include "patcher.h"

static t_list	*add_new_node(t_list **head,
							  t_list *curr,
							  uint32_t data_size)
{
	if (*head)
	{
		curr->next = malloc(sizeof(t_list));
		curr = curr->next;
	}
	else
	{
		*head = malloc(sizeof(t_list));
		curr = *head;
	}

	curr->next = NULL;
	curr->data = malloc(data_size);

	return curr;
}

static void release_list(t_list *head)
{
	if (!head)
		return ;
	if (head->next)
		release_list(head->next);
	free(head->data);
	free(head);
}

static void release_callq_list(t_list *head)
{
	t_list *p = head;
	while (p)
	{
		t_func_inf_patch *data = (t_func_inf_patch*)p->data;
		release_list(data->callq_list);
		p = p->next;
	}
	release_list(head);
}

static t_list	*get_all_function(void *file, uint8_t *handle_id)
{
	const uint64_t handle_addr = find_symbol_by_name64(file, "handle")->st_value;
	Elf64_Shdr *symtab_hdr = ((Elf64_Shdr*)get_section_by_name64(file, ".symtab", HDR));
	Elf64_Sym *symtab = (Elf64_Sym*)(file + symtab_hdr->sh_offset);

	t_list *res = NULL;
	t_list *p_res = res;
	t_function *func;

	uint8_t id = 0;
	size_t i = 0;
	while (((i + 1) * sizeof(*symtab)) <= symtab_hdr->sh_size)
	{
		if (STT_FUNC == ELF64_ST_TYPE(symtab[i].st_info))
		{
			p_res = add_new_node(&res, p_res, sizeof(t_function));

			func = (t_function*)p_res->data;
			func->addr = symtab[i].st_value;
			func->size = symtab[i].st_size;
			func->id = id++;

			if (handle_addr == func->addr)
			{
				*handle_id = func->id;
				printf("-----------------------------------------------------------------------------\n");
				printf("Handle function found, id = %d !\n", *handle_id);
				printf("-----------------------------------------------------------------------------\n");
			}
		}
		i++;
	}
	return res;
}

static void	find_all_callq(const uint64_t func_addr,
					   const uint8_t *func_body,
					   const uint64_t func_size,
					   const t_list * const func_list,
					   t_func_inf_patch *f_inf)
{
	t_list *p_curr_callq_list = f_inf->callq_list;

	uint64_t i = 0;
	while (i < func_size)
	{
		if (CALLQ_CODE == func_body[i] &&
			func_size > (i + 5))
		{
			const uint32_t jmp_offs = *((uint32_t*)(func_body + i + 1));
			const uint64_t next_addr = func_addr + i + 5;

			uint8_t id = 0;
			const t_list *p_func_list = func_list;
			while (p_func_list)
			{
				const uint32_t tmp_offs = ((t_function*)p_func_list->data)->addr - next_addr;

				if (jmp_offs == tmp_offs)
				{
					p_curr_callq_list = add_new_node(&f_inf->callq_list, p_curr_callq_list, sizeof(t_callq));

					t_callq *call_inf = (t_callq*)p_curr_callq_list->data;
					call_inf->id = id;
					call_inf->offs = i + 1;

					f_inf->inf.num++;
					i += 4;

					break;
				}

				id++;
				p_func_list = p_func_list->next;
			}
		}

		i++;
	}
}

static t_list	*get_function_calls_tables(void *file, const t_list  * const functions)
{
	const Elf64_Shdr * const text_section = ((const Elf64_Shdr * const)get_section_by_name64(file, ".text", HDR));
	t_list *res = NULL;
	t_list *p_res = res;

	const t_list *p_func = functions;
	uint8_t id = 0;
	while (p_func)
	{
		p_res = add_new_node(&res, p_res, sizeof(t_func_inf_patch));

		t_func_inf_patch *curr_func = (t_func_inf_patch*)p_res->data;
		curr_func->inf.id = id;
		curr_func->inf.num = 0;
		curr_func->callq_list = NULL;

		const uint64_t *addr = &((t_function*)p_func->data)->addr;
		const uint16_t *func_size = &((t_function*)p_func->data)->size;
		find_all_callq(*addr,
						file + (text_section->sh_offset + (*addr - text_section->sh_addr)),
						*func_size,
						functions,
						curr_func);

		id++;
		p_func = p_func->next;
	}

	return res;
}

t_status prepare_transportation(void *file)
{
	uint8_t handle_id;
	t_list *functions = get_all_function(file, &handle_id);
	if (!functions)
	{
		printf("Functions list empty !!!\n");
		return FAIL;
	}
	create_functions_table_file(functions);

	// create list of all 'callq' entry for each function
	t_list *callq_list = get_function_calls_tables(file, functions);

	if (!callq_list)
	{
		printf("Callq list empty !!!\n");
		return FAIL;
	}
	create_callq_table_file(callq_list);

	// get loader addr
	const uint64_t loader_addr = find_symbol_by_name64(file, "_start")->st_value;

	// write to file
	t_status res = write_tables(file, functions, callq_list, loader_addr, handle_id);
	if (FAIL == res)
		printf("Write tables failed !!!\n");

	release_list(functions);
	release_callq_list(callq_list);
    return res;
}