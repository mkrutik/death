#include "patcher.h"

void create_functions_table_file(const t_list * functions)
{
    const char *file_name = "Functions.csv";
    int fd = ft_open(file_name, O_CREAT | O_RDWR, 0755);
	if (-1 == fd)
	{
		printf("File <%s> creation FAILED!\n", file_name);
		return ;
	}

    uint8_t id = 0;
    while (functions)
    {
        t_function *func = (t_function*)functions->data;
        dprintf(fd, "id=%2d, addr=%7lx, size=%7d\n", func->id, func->addr, func->size);
        id++;

        functions = functions->next;
    }

    close(fd);

	size_t tab_size = (id * sizeof(t_function)) + 1; // + 1 for uint8_t number of rows
	printf("-----------------------------------------------------------------------------\n");
	printf("There are %d functions; Functions table size should be = %ld bytes\n", id, tab_size);
	printf("-----------------------------------------------------------------------------\n");
}


void create_callq_table_file(const t_list *func_inf_list)
{
    const char *file_name = "Functions_info.csv";
    int fd = ft_open(file_name, O_CREAT | O_RDWR, 0755);
	if (-1 == fd)
	{
		printf("File <%s> creation FAILED!\n", file_name);
		return ;
	}

    uint8_t rows = 0;
	uint8_t column = 0;

	while (func_inf_list)
	{
		t_func_inf_patch *func_inf = (t_func_inf_patch*)func_inf_list->data;

		if (func_inf->inf.num)
		{
			dprintf(fd, "id=%3d, num=%3d,", func_inf->inf.id, func_inf->inf.num);

			t_list *p_callq_list = func_inf->callq_list;

			for (uint8_t i = 0; i < func_inf->inf.num; i++, (p_callq_list = p_callq_list->next))
			{
				if (!p_callq_list)
				{
					printf("ERROR: callq num=%d, index=%d\n", func_inf->inf.num, i);
					return;
				}

				t_callq *callq_inf = (t_callq*)p_callq_list->data;
				dprintf(fd, " {id=%3d : offs=%5d},", callq_inf->id, callq_inf->offs);
			}

			dprintf(fd, "\n");

			rows++;
			column += func_inf->inf.num;
		}

		func_inf_list = func_inf_list->next;
	}

	close(fd);

	// + 1 for uint8_t number of functions
	size_t tabl_size = (rows * sizeof(t_func_inf_virus)) + (column * sizeof(t_callq)) + 1;

	printf("-----------------------------------------------------------------------------\n");
	printf("There are %d rows and %d column; Functions transportation table size should be = %ld bytes\n", rows, column, tabl_size);
	printf("-----------------------------------------------------------------------------\n");
}
