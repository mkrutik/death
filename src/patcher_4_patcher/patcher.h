#ifndef PATCHER_H
#define PATCHER_H

#include <stdio.h>

#include "death.h"

void        *get_section_by_name64(void *file, const char *name, char res);
Elf64_Sym   *find_symbol_by_name64(void *file, const char *name);

t_status    prepare_transportation(void *file);
t_status    write_tables(void *file, const t_list *functions, const t_list *callq, const uint64_t loader_addr, const uint8_t handle_id);

void        create_functions_table_file(const t_list * functions);
void        create_callq_table_file(const t_list *func_inf_list);

#endif