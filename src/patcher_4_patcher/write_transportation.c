#include "patcher.h"

static int write_functions_table(uint8_t *table, const t_list *functions)
{
    int res = 0;

    uint8_t *num = table;
    table++;

    t_function *p = (t_function*)table;

    while (functions)
    {
        ++(*num);

        ft_memcpy(p, functions->data, sizeof(t_function));

        p++;
        functions = functions->next;
    }

    res = *num * sizeof(t_function) + 1;

    printf("-----------------------------------------------------------------------------\n");
    printf("First table size = %d - was written\n", res);
    printf("-----------------------------------------------------------------------------\n");

    return res;
}

static int write_callq_table(uint8_t *table, const t_list *callq)
{
    int res = 0;

	uint8_t column = 0;
    uint8_t *num = table;
    table++;

    while (callq)
    {
        t_func_inf_patch *func_inf = (t_func_inf_patch*)callq->data;

        // write information for function which has internal function calls
        if (func_inf->inf.num > 0)
        {
            ++(*num);
            column += func_inf->inf.num;

            t_func_inf_virus *p = (t_func_inf_virus*)table;
            ft_memcpy(p, &func_inf->inf, sizeof(t_func_inf_virus));
            table += sizeof(t_func_inf_virus);

            t_list *callq_list = func_inf->callq_list;
            while (callq_list)
            {
                ft_memcpy(table, callq_list->data, sizeof(t_callq));
                table += sizeof(t_callq);

                callq_list = callq_list->next;
            }
        }

        callq = callq->next;
    }

    res = *num * sizeof(t_func_inf_virus) + column * sizeof(t_callq) + 1;

    printf("-----------------------------------------------------------------------------\n");
    printf("Second table size = %d - was written\n", res);
    printf("-----------------------------------------------------------------------------\n");

    return res;
}

t_status write_tables(void *file,
                      const t_list *functions,
                      const t_list *callq,
                      const uint64_t loader_addr,
                      const uint8_t handle_id)
{
    Elf64_Shdr  *text;
    Elf64_Sym   *enc_sym;
    size_t      offset;
    uint8_t     *tables;

    if (NULL == (enc_sym = find_symbol_by_name64(file, "tables")) ||
        NULL == (text = get_section_by_name64(file, ".text", HDR)))
    {
        return FAIL;
    }

	offset = text->sh_offset + (enc_sym->st_value - text->sh_addr);
	tables = (uint8_t*)(file + offset);

    // write "handle" function id in table
    *tables = handle_id;
    tables++;

    // write start symbol phisical addr
    *((uint64_t*)tables) = loader_addr;
    tables += sizeof(loader_addr);

    int counter = write_functions_table(tables, functions);
    if (0 == counter ||
        0 == write_callq_table(tables + counter, callq))
    {
        return FAIL;
    }

    return SUCCESS;
}