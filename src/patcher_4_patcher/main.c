/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <mkrutik@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/21 14:58:52 by adzikovs          #+#    #+#             */
/*   Updated: 2019/10/05 15:24:34 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "patcher.h"

void			*get_section_by_name64(void *file, const char *name, char res)
{
	size_t		i;
	Elf64_Shdr	*shstrtable_hdr;
	char		*shstrtable;

	shstrtable_hdr = SHDRS64(file) + ((Elf64_Ehdr*)file)->e_shstrndx;
	shstrtable = (char*)(file + shstrtable_hdr->sh_offset);
	i = 0;
	while (i < ((Elf64_Ehdr*)file)->e_shnum)
	{
		if (ft_strcmp(shstrtable + SHDRS64(file)[i].sh_name, name) == 0)
		{
			if (res == HDR)
				return (SHDRS64(file) + i);
			else
				return (file + SHDRS64(file)[i].sh_offset);
		}
		i++;
	}
	return (NULL);
}

Elf64_Sym		*find_symbol_by_name64(void *file, const char *name)
{
	Elf64_Shdr	*symtab_hdr;
	Elf64_Sym	*symtab;
	char		*strtab;
	size_t		i;

	symtab_hdr = ((Elf64_Shdr*)get_section_by_name64(file, ".symtab", HDR));
	symtab = (Elf64_Sym*)(file + symtab_hdr->sh_offset);
	strtab = get_section_by_name64(file, ".strtab", SECT);
	i = 0;
	while (((i + 1) * sizeof(*symtab)) <= symtab_hdr->sh_size)
	{
		if (ft_strcmp(strtab + symtab[i].st_name, name) == 0)
			return (symtab + i);
		i++;
	}
	return (NULL);
}

t_status		update_text_size(void *file)
{
	Elf64_Sym	*op_sym;
	Elf64_Shdr	*text;
	size_t		offset;
	uint64_t	*arg_ptr;

	if ((op_sym = find_symbol_by_name64(file, "text_size")) == NULL ||
		(text = get_section_by_name64(file, ".text", HDR)) == NULL ||
		op_sym->st_value < text->sh_addr)
		return (FAIL);
	offset = text->sh_offset + (op_sym->st_value - text->sh_addr);
	arg_ptr = (uint64_t*)(file + offset);
	*arg_ptr = text->sh_size;
	return (SUCCESS);
}

t_status		encrypt_text_section(void *file)
{
	Elf64_Sym		*enc_sym;
	Elf64_Sym		*dec_sym;
	Elf64_Sym		*fingerprint_sym;

	Elf64_Shdr		*text;
	size_t			offset;
	size_t			smb_offset;
	unsigned char	*enc_ptr;
	unsigned char	*dec_ptr;
	char			*fingerprint_ptr;

	if ((enc_sym = find_symbol_by_name64(file, "enc_start")) == NULL ||
		(dec_sym = find_symbol_by_name64(file, "dec_start")) == NULL ||
		(fingerprint_sym = find_symbol_by_name64(file, "fingerprint")) == NULL ||
		(text = get_section_by_name64(file, ".text", HDR)) == NULL ||
		enc_sym->st_value < text->sh_addr ||
		dec_sym->st_value < text->sh_addr ||
		fingerprint_sym->st_value < text->sh_addr)
	{
		return (FAIL);
	}

	smb_offset = enc_sym->st_value - text->sh_addr;
	offset = text->sh_offset + smb_offset;
	enc_ptr = (unsigned char*)(file + offset);

	smb_offset = dec_sym->st_value - text->sh_addr;
	offset = text->sh_offset + smb_offset;
	dec_ptr = (unsigned char*)(file + offset);

	smb_offset = fingerprint_sym->st_value - text->sh_addr;
	offset = text->sh_offset + smb_offset;
	fingerprint_ptr = (char*)(file + offset);

	smb_offset = enc_sym->st_value - text->sh_addr;
	encryption(enc_ptr, (text->sh_size - smb_offset), dec_ptr);

	md5((unsigned char*)(file + text->sh_offset), text->sh_size, fingerprint_ptr);
	return (SUCCESS);
}

int				main(int argc, char *argv[])
{
	t_file		bin;

	if (argc == 2)
	{
		bin.file_name = argv[1];
		if (SUCCESS != get_file_content(&bin) ||
				SUCCESS != elf_64_parse(&bin))
			printf("%s\n", "File check failed!");
		else if (FAIL == update_text_size(bin.data))
			printf("%s\n", "Patching failed!");
		else if (FAIL == prepare_transportation(bin.data))
			printf("%s\n", "Preparing for transportation failed!");
#ifndef ENC_OFF
		else if (FAIL == encrypt_text_section(bin.data))
			printf("%s\n","Encryption failed!");
#endif
		else
		{
			ft_munmap(bin.data, bin.mem_size);
			return (0);
		}
	}
	return (EXIT_FAILURE);
}
