/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   file_helpers.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <mkrutik@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/21 15:02:45 by adzikovs          #+#    #+#             */
/*   Updated: 2019/10/01 22:12:07 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "death.h"

t_status	change_file_size(const char *name, int mod)
{
	struct stat		buff;

	if (mod)
	{
		if (ft_stat(name, &buff, 0))
			return (FAIL);
		if (mod < 0 && buff.st_size < (long int)((-1) * mod))
			return (FAIL);
		if (ft_truncate(name, (off_t)(buff.st_size + mod)))
			return (FAIL);
	}
	return (SUCCESS);
}

int	mmap_check(void *ret)
{
	return ((ret) == (void*)-EACCES || (ret) == (void*)-EAGAIN ||
			(ret) == (void*)-EBADF || (ret) == (void*)-EEXIST ||
			(ret) == (void*)-EINVAL || (ret) == (void*)-ENFILE ||
			(ret) == (void*)-ENODEV || (ret) == (void*)-ENOMEM ||
			(ret) == (void*)-EOVERFLOW || (ret) == (void*)-EPERM ||
			(ret) == (void*)-ETXTBSY || (ret) == MAP_FAILED);
}

t_status	get_file_content(t_file *out)
{
	int		fd;
	int		size;

	fd = -1;
	if (out == NULL || out->file_name == NULL)
		return (FAIL);
	if ((fd = ft_open(out->file_name, O_RDWR, 0755)) > 2)
	{
		if ((size = ft_lseek(fd, 0, SEEK_END)) != -1)
		{
			out->size = (size_t)size;
			out->mem_size = out->size + (PAGE_SIZE - out->size % PAGE_SIZE);
			out->data = ft_mmap(NULL, out->mem_size, PROT_READ | PROT_WRITE,
								MAP_SHARED, fd, 0);
			ft_close(fd);
			if (mmap_check(out->data))
			{
				return (FAIL);
			}
			return (SUCCESS);
		}
	}
	return (FAIL);
}

void	get_random_bytes(uint8_t *out, size_t size)
{
	const char	random[] = "/dev/urandom";
	int		fd;

	if ((fd = ft_open(random, O_RDONLY, 0666)) != -1)
	{
		ft_read(fd, out, size);
		ft_close(fd);
	}
}