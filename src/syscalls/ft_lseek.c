#include "death.h"

off_t   ft_lseek(int fd, off_t offset, int whence)
{
    (void) fd;
    (void) offset;
    (void) whence;

    off_t __res; 
    __asm__ volatile ("syscall"
                 :"=a"(__res)
                 :"0" ((long)(8)));
    return __res;
}