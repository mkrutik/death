#include "death.h"

ssize_t ft_read(int fd, const void *buf, size_t count)
{
    (void) fd;
    (void) buf;
    (void) count;

    ssize_t __res;
    __asm__ volatile ("syscall"
                :"=a"(__res)
                :"0" ((long)(0)));
    return __res;
}