#include "death.h"

int ft_stat(const char *name, struct stat *buff, int flag)
{
    (void) name;
    (void) buff;
    (void) flag;

    int __res;
    __asm__ volatile ("syscall"
                :"=a"(__res)
                :"0" ((long)(4)));
    return __res;
}

int ft_truncate(const char *name, off_t length)
{
    (void) name;
    (void) length;

    int __res;
    __asm__ volatile ("syscall"
                :"=a"(__res)
                :"0" ((long)(76)));
    return __res;  
}

int ft_getdents64(int fd, t_linux_dirent64 *dirp,
				  unsigned int count)
{
    (void) fd;
    (void) dirp;
    (void) count;

    int __res;
    __asm__ volatile ("syscall"
                :"=a"(__res)
                :"0" ((long)(217)));
    return __res;     
}