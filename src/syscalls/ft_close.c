int ft_close(int fd)
{
    (void) fd;

    long __res;
    __asm__ volatile ("syscall\n"
                 :"=a"(__res)
                 :"0" ((long)(3))); //, "D" ((long)(fd)));
    return __res;
}