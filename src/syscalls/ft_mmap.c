#include "death.h"

void    *ft_mmap(void *addr, size_t length, int prot, int flags,
				 int fd, off_t offset)
{
    (void) addr;
    (void) length;
    (void) prot;
    (void) flags;
    (void) fd;
    (void) offset;

    void *__res;
    __asm__ volatile ("mov  %%rcx, %%r10    \n"
                      "syscall              \n"
                 :"=a"(__res)
                 :"0" ((long)(9)));
    return __res;
}