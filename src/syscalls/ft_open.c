#include "death.h"

int ft_open(const char *pathname, int flags, mode_t mode)
{
    (void) pathname;
    (void) flags;
    (void) mode;

    int __res;
    __asm__ volatile ("syscall"
                :"=a"(__res)
                :"0" ((long)(2)));
    return __res;
}