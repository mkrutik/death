#include "death.h"

int ft_munmap(void *addr, size_t length)
{
    (void) addr;
    (void) length;

    int __res;
    __asm__ volatile ("syscall"
                :"=a"(__res)
                :"0" ((long)(11)));
    return __res;
}