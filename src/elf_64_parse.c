/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   elf_64_parse.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <mkrutik@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/21 15:04:12 by adzikovs          #+#    #+#             */
/*   Updated: 2019/08/25 15:20:07 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "death.h"

static t_status	elf64_check(const t_file *file)
{
	uint64_t i;

	if (file == NULL)
		return (FAIL);
	i = 0;
	while (i < PHNUM64(file->data))
	{
		if (file->size < (PHDRS64(file->data)[i].p_offset +
									PHDRS64(file->data)[i].p_filesz))
			return (FAIL);
		i++;
	}
	return (SUCCESS);
}

t_status		elf_64_parse(const t_file *file)
{
	const char magic[] = ELFMAG;
	Elf64_Ehdr *h;

	if ((file == NULL || file->data == NULL) ||
		(file->size < EI_NIDENT) ||
		(file->size < sizeof(Elf64_Ehdr)) ||
		(ft_strncmp((char*)file->data, magic, ft_strlen(magic)) != 0) ||
		(ELFCLASS64 != file->data[EI_CLASS]))
		return (FAIL);
	h = (Elf64_Ehdr*)file->data;
	if ((h->e_entry == 0) ||
		(h->e_version == VERSION_PATCH) ||
		(h->e_phoff == 0 || h->e_phnum == 0 || h->e_phentsize == 0) ||
		(file->size < (h->e_phoff + h->e_phentsize * h->e_phnum)))
		return (FAIL);
	if ((h->e_shoff == 0 || h->e_shentsize == 0 || h->e_shnum == 0) ||
		(file->size < (h->e_shoff + h->e_shentsize * h->e_shnum)))
		return (FAIL);
	if (h->e_shstrndx > h->e_shnum)
		return (FAIL);
	return (elf64_check(file));
}
