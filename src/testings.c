#include "death.h"

void ft_printnbr(uint64_t x, int base)
{
	char buff[(sizeof(base) * 8 + 1)];
	const char range[] = "0123456789abcdef";

	int last = 0;
	do
	{
		buff[last] = range[x % base];
		x /= base;
		last++;
	} while (x != 0);
	buff[last] = '\0';

	int len = last;
	last--;

	int i = 0;
	while (i <= last)
	{
		char val = buff[i];
		buff[i] = buff[last];
		buff[last] = val;

		i++;
		last--;
	}

	ft_write(1, buff, len);
}

void print_table(uint64_t tables)
{
	uint8_t *addr = (uint8_t*)tables;
	// first table handle
	uint8_t num = *addr;
	addr++;

	ft_printnbr(num, 10);
	ft_write(1, "\n", 1);
	uint8_t i = 0;
	while (i < num)
	{
		t_function *tmp = (t_function*)addr;

		ft_printnbr(tmp->id, 10);
		ft_write(1, "\t", 1);

		ft_printnbr(tmp->addr, 16);
		ft_write(1, "\t", 1);

		ft_printnbr(tmp->size, 10);
		ft_write(1, "\n", 1);

		addr += sizeof(t_function);
		i++;
	}

	// second table
	num = *addr;
	addr++;
	ft_printnbr(num, 10);
	ft_write(1, "\n", 1);

	i = 0;
	while (i < num)
	{
		t_func_inf_virus *inf = (t_func_inf_virus*)addr;
		addr += sizeof(t_func_inf_virus);

		ft_printnbr(inf->id, 10);
		ft_write(1, " ", 1);
		ft_printnbr(inf->num, 10);
		ft_write(1, " {", 2);

		uint8_t j = 0;
		while (j < inf->num)
		{
			t_callq *cal = (t_callq*)addr;
			addr += sizeof(t_callq);

			ft_write(1, "(", 1);
			ft_printnbr(cal->id, 10);
			ft_write(1, " ", 1);
			ft_printnbr(cal->offs, 10);
			ft_write(1, ") ", 2);

			j++;
		}

		ft_write(1, "\n", 1);
		i++;
	}
}