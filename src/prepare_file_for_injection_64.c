/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   prepare_file_for_injection_64.c                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <mkrutik@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/21 15:00:32 by adzikovs          #+#    #+#             */
/*   Updated: 2019/10/01 21:56:28 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "death.h"

t_status		find_taget_segment_64(const void *file, t_target_phdr *res)
{
	Elf64_Phdr	*phdr;

	phdr = PHDRS64(file);
	res->i = 0;
	while (res->i < PHNUM64(file))
	{
		if (ENTRY(file) >= phdr[res->i].p_vaddr &&
			ENTRY(file) < phdr[res->i].p_vaddr + phdr[res->i].p_filesz)
		{
			res->hdr = phdr[res->i];
			return (SUCCESS);
		}
		res->i++;
	}
	return (FAIL);
}

static void		elf_64_change_phdr(void *file, Elf64_Phdr *target)
{
	Elf64_Phdr	*phdr;
	uint64_t	i;

	phdr = PHDRS64(file);
	i = 0;
	while (i < PHNUM64(file))
	{
		if (phdr[i].p_offset >= (target->p_offset + target->p_filesz))
			phdr[i].p_offset += PAGE_SIZE;
		i++;
	}
}

static void		elf_64_change_shdr(void *file, Elf64_Phdr *target, uint64_t text_size)
{
	Elf64_Shdr	*shdr;
	size_t		i;

	shdr = SHDRS64(file);
	i = 0;
	while (i < SHNUM64(file))
	{
		if (shdr[i].sh_offset >= (target->p_offset + target->p_filesz))
			shdr[i].sh_offset += PAGE_SIZE;
		// target section
		else if ((shdr[i].sh_offset + shdr[i].sh_size) == (target->p_offset + target->p_filesz))
			shdr[i].sh_size += text_size;
		i++;
	}
}

t_status		prepare_file_for_injection64(t_file *file, Elf64_Phdr *target, uint64_t text_size)
{
	void	*dst;
	void	*src;
	size_t	offset;
	size_t	size;

	elf_64_change_phdr(file->data, target);
	elf_64_change_shdr(file->data, target, text_size);
	offset = target->p_offset + target->p_filesz;
	dst = file->data + offset + PAGE_SIZE;
	src = file->data + offset;
	size = file->size - offset - PAGE_SIZE;
	ft_memmove(dst, src, size);
	((Elf64_Ehdr*)file->data)->e_shoff += PAGE_SIZE;
	return (SUCCESS);
}
