/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   defines.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <mkrutik@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/26 17:19:11 by mkrutik           #+#    #+#             */
/*   Updated: 2019/10/02 20:39:53 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef DEFINES_H
# define DEFINES_H
# include <errno.h>

# define LOG(str) ft_write(1, str, sizeof(str) - 1);
# undef LOG
# define LOG(str) ;

# define VERSION_PATCH 0x12345678
# define PAGE_SIZE 0x5000 // fucking hard coding

# define BUFF_SIZE 1024

# define HDR 0
# define SECT 1

# define ENTRY(p) (((Elf64_Ehdr*)p)->e_entry)
# define PHNUM64(p) (((Elf64_Ehdr*)p)->e_phnum)
# define PHDRS64(p) ((Elf64_Phdr*)((void*)p + ((Elf64_Ehdr*)p)->e_phoff))
# define SHNUM64(p) (((Elf64_Ehdr*)p)->e_shnum)
# define SHDRS64(p) ((Elf64_Shdr*)((void*)p + ((Elf64_Ehdr*)p)->e_shoff))

#endif
