#ifndef TRANSPORTATION_H
#define TRANSPORTATION_H

#include <stdint.h>

#define CALLQ_CODE (uint8_t)0xE8
#define JUMP_SIZE 4

typedef struct      s_list
{
    void            *data;
    struct s_list   *next;
}                   t_list;

typedef struct      s_function
{
    uint64_t        addr;
    uint16_t        size;
    uint8_t         id;
}                   t_function;

typedef struct      s_func_inf_virus
{
    uint8_t         id;
    uint8_t         num;
}                   t_func_inf_virus;

// Note : t_func_inf_patch structure used only in patcher
typedef struct      s_func_inf_patch
{
    t_func_inf_virus inf;
    t_list          *callq_list;
}                   t_func_inf_patch;

typedef struct      s_callq
{
    uint16_t        offs;
    uint8_t         id;
}                   t_callq;

// used only for testing
void print_table(uint64_t tables);
void ft_printnbr(uint64_t x, int base);

#endif