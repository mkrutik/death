FROM debian:8


RUN apt-get update; \
    apt-get install -y \
    nasm \
    gcc \
    make \
    binutils \
    file \
    strace \
    vim \
    valgrind \
    gdb

# docker build -t death .
# docker run --cap-add=SYS_PTRACE --security-opt seccomp=unconfined
