# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: mkrutik <mkrutik@student.unit.ua>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/05/22 21:35:20 by mkrutik           #+#    #+#              #
#    Updated: 2019/10/05 15:26:12 by mkrutik          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #


####################### Configuration ##########################################

# To build without encryption: 				make USER_DEFINES="-DENC_OFF"
# To build without checking for debugging:	make USER_DEFINES="-DANTI_DEBUG"
# USER_DEFINES = -DENC_OFF -DANTI_DEBUG

CC = gcc
CFLAGS = -Wall \
		 -Wextra \
		 -Werror \
		 -fPIC \
		 -m64 \
		 -fno-stack-protector \
		 -fno-asynchronous-unwind-tables \
		 -std=gnu99 \
		 $(USER_DEFINES)

ASM = nasm
AFLAGS = -f elf64 $(USER_DEFINES)

LD = ld
LDFLAGS = --gc-sections \
		  --print-gc-sections \
		  -pie \
		  --dynamic-linker /lib64/ld-linux-x86-64.so.2

####################### Directories names ######################################

INC = includes
SDIR = src
ODIR = Objects

####################### Common files ###########################################

HEADERS1 = death.h \
		   defines.h

HEADERS_R = $(HEADERS:%.h=$(INC)/%.h)

COMMON_SRCS = syscalls/ft_open.c \
			  syscalls/ft_close.c \
			  syscalls/ft_lseek.c \
			  syscalls/ft_mmap.c \
			  syscalls/ft_munmap.c \
			  syscalls/ft_read.c \
			  syscalls/syscalls.c \
			  elf.c \
			  elf_64.c \
			  elf_64_parse.c \
			  file_helpers.c \
			  libft_helpers.c \
        	  prepare_file_for_injection_64.c \
			  encryption.c \
			  md5.c \
			  transportation.c
			#   syscalls/ft_write.c \
			#   testings.c \

####################### Main binary ############################################

NAME = Death

# Warning loader.asm - must be first file in the src list
SRCS =	asm/loader.asm \
		$(COMMON_SRCS)

SRCS_R = $(SRCS:%=$(SDIR)/%)
OBJ_T  = $(SRCS:%.c=%.o)
OBJ    = $(OBJ_T:%.asm=%.o)
OBJ_R  = $(OBJ:%.o=$(ODIR)/%.o)

####################### Helper binary ##########################################

NAME_2 = patcher_for_patcher

SRCS_2 = $(COMMON_SRCS) \
		patcher_4_patcher/main.c \
		patcher_4_patcher/prepare_transportation.c \
		patcher_4_patcher/create_info_tables.c \
		patcher_4_patcher/write_transportation.c

SRCS_R_2 = $(SRCS_2:%=$(SDIR)/%)
OBJ_T_2  = $(SRCS_2:%.c=%.o)
OBJ_2    = $(OBJ_T_2:%.asm=%.o)
OBJ_R_2  = $(OBJ_2:%.o=$(ODIR)/%.o)

##################### Targets and Rules ######################################

.PHONY: all clean fclean re norm add pr_test setup2 pr_test2

all : $(NAME)

$(NAME)_build : $(ODIR) $(OBJ_R) $(NAME_2)
	$(LD) $(LDFLAGS) -o $(NAME) $(OBJ_R)

$(NAME) : $(NAME)_build
	./$(NAME_2) $(NAME)
	objcopy --only-keep-debug $(NAME) $(NAME).dSym
	# strip --strip-all $(NAME)
	objcopy --add-gnu-debuglink=$(NAME).dSym $(NAME)

$(NAME_2) : $(ODIR) $(OBJ_R_2)
	$(CC) -o $(NAME_2) $(OBJ_R_2)


clean :
	rm -f $(OBJ_R)
	rm -rf $(ODIR)

fclean : clean
	rm -f $(NAME) $(NAME).dSym
	rm -rf $(ODIR)
	rm -f patcher_for_patcher

re : fclean $(NAME)

norm:
	make norm -C libft
	norminette $(SRCS_R) $(HEADERS_R)

add:
	make add -C libft
	git add $(SRCS_R) $(HEADERS_R)\
			.gitignore Makefile CMakeLists.txt author

$(ODIR)/%.o : $(SDIR)/%.c
	$(CC) $(CFLAGS) -c $< -o $@ -I $(INC)

$(ODIR)/%.o : $(SDIR)/%.asm
	$(ASM) $(AFLAGS) -o $@ $<

$(ODIR):
	mkdir -p $@
	mkdir -p $@/asm
	mkdir -p $@/syscalls
	mkdir -p $@/patcher_4_patcher

pr_test: re
	cp /bin/ls /tmp/ls
	./$(NAME)
	mv /tmp/ls ./ls_patched
	cp /bin/ls /tmp/ls

setup2: re
	mkdir -p /tmp/test
	mkdir -p /tmp/test2
	cp /bin/ls /tmp/test
	cp /usr/bin/strings /tmp/test2

pr_test2: setup2
	./$(NAME)
	mv /tmp/test/ls ./ls_patched
	mv /tmp/test2/strings ./strings_patched
	cp /bin/ls /tmp/test
	cp /usr/bin/strings /tmp/test2


 #ln -s /lib64/ld-linux-x86-64.so.2 /lib/ld64.so.1