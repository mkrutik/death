#!/bin/sh
# make re

# clear previous
rm -f /tmp/Death
rm -rf /tmp/test /tmp/test2

# prepare everything
cp Death /tmp
mkdir -p /tmp/test /tmp/test2
cp /bin/ls /tmp/test
cp /bin/ls /tmp/test2

# execute virus
/tmp/Death

# check infected binaries
strings /tmp/test/ls | grep mkr
strings /tmp/test2/ls | grep mkr

# check that infect binary also infect another binaries
cp /bin/ls /tmp/test2/new

/tmp/test/ls -l
strings /tmp/test2/new | grep mkr

/tmp/test2/new -la


objdump -D -z Death          > death
objdump -D -z /tmp/test/ls   > first_ls
objdump -D -z /tmp/test2/ls  > second_ls