#!/bin/sh
if test "$#" -eq 1; then
	NAME1=/bin/$1
	NAME2=/tmp/$1
elif test "$#" -eq 2; then
	NAME1=$1
	NAME2=$2
else
	exit
fi
readelf -aW $NAME1 > orig.txt
readelf -aW $NAME2 > modified.txt
vimdiff orig.txt modified.txt
